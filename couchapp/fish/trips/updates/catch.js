/**
 * Update Catch
 */
function (doc, req) { // jshint ignore: line
	const moment = require("lib/moment");
	const utils = require("lib/utils");
	
	const now = moment().toISOString();
	
	doc = doc || {
		_id : req.form.trip,
		catches : [],
		waypoints : [],
	};
	
	//setup the meta data
	doc.meta = doc.meta || {
		created : now
	};
	doc.meta.modified = now;
	doc.meta.type = 'trip';
	doc.meta.by = req.userCtx.name || "un-authed";
	
	//attempt to make this look as much like a fishing trip as possible
	//in otherwords... initialize what we can
	doc.fisherman = doc.fisherman || req.userCtx.name;
	doc.catches = doc.catches || [];
	
	var catchid = 0;
	try{
		catchid = Object.keys(req.query)[0];
		catchid = parseInt(catchId);
	}catch(ex){
		catchid = 0;
	}
	
	if(doc.catches.length <= catchid){
		catchid = doc.catches.length;
		doc.catches.push({});
	}
	var c = doc.catches[catchid];
	
	c.timestamp = moment(req.form.timestamp).toISOString() || now;
	c.fish = c.fish || {};
	c.fish.species = (req.form.species || c.fish.species || "").trim() || undefined;
	c.fish.weight = utils.toApproxNum(req.form.weight) || c.fish.weight || undefined;
	c.fish.length = utils.toApproxNum(req.form.length) || c.fish.length || undefined;
	c.fish.girth = utils.toApproxNum(req.form.girth) || c.fish.girth || undefined;
	
	// so, if we've added a new catch, we may need to adjust our waypoints
	while(doc.waypoints.length < 2) doc.waypoints.push(c);
	doc.waypoints.sort(function(a,b){
		a = moment(a);
		b = moment(b);
		return a.diff(b);
	});
	if(c.timestamp < doc.waypoints[0].timestamp){
		doc.waypoints.unshift(c);
	}
	doc.waypoints.reverse();
	if(c.timestamp > doc.waypoints[0].timestamp){
		doc.waypoints.unshift(c);
	}
	
	// draw the thing
	this.renderer = eval("("+this.shows.catch+")");
	var rendering = this.renderer(doc,req);
	return [doc, rendering];
}
