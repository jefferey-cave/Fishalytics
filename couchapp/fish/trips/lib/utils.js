module.exports = new (function(){ // jshint ignore:line
	'use strict';

	this.getBaseUrl = function(req){
		return this.getBasePath(req).join('/');
	};

	this.getBasePath = function(req){
		var BaseUrl = req.headers.Referer;
		BaseUrl = !(BaseUrl && BaseUrl.split(':')[0] === 'https') ? "https:/" : "https:/";
		BaseUrl = [
				BaseUrl
				,req.headers.Host
			];
		if(req.requested_path[1] === '_design'){
			BaseUrl = BaseUrl.concat(req.requested_path.slice(0,4));
		}
		return BaseUrl;
	};

	/**
	 *
	 */
	this.toApproxNum = function(o){
		var n = null;
		try{
			n = Math.abs(parseInt(o,10));
			var e = (o||"").slice(-1)=="~"?"~":"";
			if(e){
				n = n + e;
			}
			return n;
		}
		catch(ex){
			n = null;
		}
		return n;
	};
})();
