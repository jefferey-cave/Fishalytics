/* global emit */
// jshint -W025
/**
 * bySpecies Maps
 *
 * Within our catches we record the species caught.
 *
 */
function(doc) {
	'use strict';

	var catchUtils = require('views/lib/catch');

	var $catches =catchUtils.asCatches(doc);
	for(var $c in $catches){
		var $key = $catches[$c];
		$key = $key.val;
		$key = JSON.parse(JSON.stringify($key));
		$key = [
			$key.fish.species,
			$key.coords[0],
			$key.coords[1],
			$key.timestamp
		];

		var $val = JSON.parse(JSON.stringify($catches[$c].val));
		delete $val.licenses;
		delete $val.status;
		delete $val.rig;
		delete $val.stats.lines;
		delete $val.stats.span;

		$val.weight = parseInt($val.fish.weight,10);
		if(isNaN($val.weight)){
			$val.weight = null;
		}
		$val.length = parseInt($val.fish.length,10);
		if(isNaN($val.length)){
			$val.length = null;
		}

		emit($key,$val);
	}

}
