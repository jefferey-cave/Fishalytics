[![build status](https://gitlab.com/jefferey-cave/Fishalytics/badges/master/build.svg)](https://gitlab.com/jefferey-cave/Fishalytics/commits/master)

# FishAlytics

> ** With the end of Smileupps, so came the end of Fishalytics **
> 
> It was a fun 10 years, and a lot of fish were caught, and a lot of bodies of water mapped... but all good things come to an end.
>
> While the free version of Fishalytics is over, it was always 
> designed with governmental, or professional fisheries management 
> in mind. 
> 
> Unfortunately, there just isn't much demand for data-analysis in fish and game management.
> 
> If you are interested in turning your fisheries data into 
> meaningful analytics, send me a message.


Catch more fish by co-operatively tracking and sharing sport angling data.

(and improve ecological management at the same time)


![](/couchapp/fish/trips/_attachments/intro/img/about2.png)

Fishing tales are the stuff legends are made of. Told over a couple of pints, 
stories have been told about where the fish are, where they can be found, where 
the biggest fish has been caught. We all love our fishing tales, but there is 
one problem with them: most of them are just that... tales.

We mean well, but we are human; as time passes our memories change and fade. 
Tales of glory get more glorious; tales of failure recede from our memories: in 
other words we can't trust them. Which is fine, until you need to plan your 
next fishing trip.

> You find gold where the gold is
> 
> - Prospector's Proverb

## The Solution

Serious fisherman know that nothing beats accurate record keeping for knowing 
how and where to catch that next fish. It allows us to identify and know how 
fish behave and where they are likely to be found.

In the past, this meant paper journals spanning decades, but in the 21st 
century, that means FishAlytics. Rather than just looking across years, we 
offer the ability to compare data from multiple fisherman about conditions 
*right now*.

Where do you need to cast your line to find your next fish? You find fish where 
the fish are. FishAlytics knows where they are.

### Fisheries Management

Turn fishermen's stories into meaningful analytics.

Don't wait until the end of the "season" to get paper records sent to 
you. Use catch data collected by your customers in real-time to gain 
insight into the health of your bodies of water.

Compare Length-Weight (log a vs b) and catch rates per hour across multiple 
years to gain serious insight into the health of your fishery, and identify 
needs for early intervention. 

Compare this data to 
[international, standardized](https://www.fishbase.se/manual/english/FishBaseThe_LENGTH_WEIGHT_Table.htm), 
studies to understand your water's health. Share this data back to further the study.

### Ecology

Fish populations are early warning signs of ecological disasters. Changes 
in types and sizes of fish populations is a good indicator of ecological 
health of the water. FishAlytics gives real-time, and early detection of 
ecological issues.

Fisherman don't want to fish an overfished area. Ecologist don't want 
fishing happening in stressed areas. FishAlytics directs identifies healthy 
populations of fish and directs fisherman toward those; leaving stressed 
populations alone to recover.
