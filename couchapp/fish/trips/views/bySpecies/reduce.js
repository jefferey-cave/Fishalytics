/**
 * Reduce bySpecies
 *
 */
// jshint -W025
function (key, values, rereduce) {
	//log("0:"+JSON.stringify(rereduce));

	function stats(key, values, rereduce){
		var $stat = {
			sum:0
			,count:0
			,avg:0
		};
		values.forEach(function(val){
			if(rereduce){
				//log("1:"+JSON.stringify(val));
				$stat.sum += val.sum || 0;
				$stat.count += val.count || 0;
			}
			else{
				if(val!==null){
					$stat.sum += val;
					$stat.count++;
				}
				else{
					log("2:"+JSON.stringify(val));
				}
			}
		});
		if($stat.count !== 0){
			$stat.avg = $stat.sum/$stat.count;
		}
		return $stat;
	}

	var $val = {
		timestamp:[]
		,weight:[]
		,length:[]
	};

	//log("6: "+JSON.stringify(values));
	values.forEach(function(val){
		if(!rereduce){
			val.timestamp = new Date(val.timestamp).getTime();
		}
		$val.timestamp.push(val.timestamp);
		$val.weight.push(val.weight);
		$val.length.push(val.length);
		//log("4:"+JSON.stringify(val));
	});
	//log("3: "+JSON.stringify($val));
	$val.timestamp = stats(null,$val.timestamp,rereduce);
	$val.weight = stats(null,$val.weight,rereduce);
	$val.length = stats(null,$val.length,rereduce);
//	$val.lwFactor = stats(null,$val.lwFactor,rereduce);

	//log("5: "+JSON.stringify($val));
	return $val;
}
