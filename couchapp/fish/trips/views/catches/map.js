/*
global catchUtils
global emit
*/
// jshint -W025
/**
 * Catches
 */
function(doc) {
	'use strict';

	var catchUtils = require('views/lib/catch');
	var catches = catchUtils.asCatches(doc);
	catches.forEach(function($catch){
		emit($catch.key,$catch.val);
	});
}
